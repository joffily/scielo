import io
import os
from flask import Flask
from flask_restful import Api
from flask_cors import CORS
from controllers.stylechecker import StyleCheckerResource


app = Flask(__name__)
DEBUG_MODE = os.environ.get('DEBUG_MODE', False)

if DEBUG_MODE:
    cors = CORS(app, resources={r"/*": {"origins": "*"}})

api = Api(app)
api.add_resource(StyleCheckerResource, "/validator/stylechecker")


if __name__ == "__main__":
    app.run(debug=DEBUG_MODE)
