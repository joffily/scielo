import io
import os
import unittest
from app import app as flaskApp


class StyleCheckerTests(unittest.TestCase):

    def setUp(self):
        self.app = flaskApp.test_client()
        self.invalid_file = {'article': (io.BytesIO(b'<b></b>'), 'test.xml')}

        with open("".join([os.getcwd(), '/tests/assets/samplexml.xml']), "r") as file:
            content = file.read()
            self.valid_file = {'article': (io.BytesIO(
                bytes(content, 'UTF-8')), 'test.xml')}

    def test_should_respond_with_bad_request(self):
        response = self.app.post('/validator/stylechecker')
        self.assertEqual(response.status_code, 400)

    def test_should_require_a_archive(self):
        response = self.app.post('/validator/stylechecker')
        expected = {'message':
                    {'article': 'Missing required parameter in an uploaded file'}}
        self.assertEqual(response.json, expected)

    def test_should_respond_with_200_status_code(self):
        response = self.app.post('/validator/stylechecker',
                                 buffered=True, data=self.invalid_file)
        self.assertEqual(response.status_code, 200)

    def test_should_be_an_invalid_xml_file(self):
        response = self.app.post('/validator/stylechecker',
                                 buffered=True, data=self.invalid_file)

        self.assertFalse(response.json.get("is_valid"))
        self.assertEqual(response.json.get("errors")[-1], "Invalid file")

    def test_should_be_a_valid_xml_file(self):
        response = self.app.post('/validator/stylechecker',
                                 buffered=True, data=self.valid_file)

        self.assertFalse(response.json.get("is_valid"))

