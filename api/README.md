# SciELO - StyleChecker API

It is a simple packtools stylechecker wrapper using HTTP to expose these functionality.

## Endpoints

- `/validator/stylechecker` - Check if a submited file is a valid file based on SciELO style checker
	* Parameters
		- archive

## Installing

Before running this project you shall install its dependencies, make sure you have a dedicated `virtualenv` and `Python 3.5+` installed. To install you should activate your virtualenv and run this command:

```shell
pip install -r requirements.txt
```

## Running the API

This project use `Flask` as web framework. Running this app is easy-peasy, see the magic happening with this command:

```shell
DEBUG_MODE=True python app.py
```

Take attention to `DEBUG_MODE` env variable, when we are in development env its nice to set it as `True` because we're trying to accept all cors solicitation and of course get the hot reload of Flask.

## Running tests

This project use `pytest` to automatically detect tests and run their like a charm. If you want to run all integration tests use following command:

```shell
pytest
```