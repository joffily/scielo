import io
import os
from flask_restful import Resource, reqparse
from werkzeug import FileStorage
from packtools import XMLValidator, stylechecker
from packtools.catalogs import XML_CATALOG


os.environ['XML_CATALOG_FILES'] = XML_CATALOG


def stylechecker_validator(xml):
    """Validate that xml file using packtools
    ::xml io.BytesIO"""
    try:
        validator = XMLValidator.parse(xml)
        return stylechecker.summarize(validator)
    except Exception:
        return {"is_valid": False, "errors": ["Invalid file"]}


class StyleCheckerResource(Resource):
    """Style Checker Resource receive a HTTP Post with
    a file body and validate with SciELO's stylechecker tool"""

    def post(self):
        parse = reqparse.RequestParser()
        parse.add_argument('article', required=True, type=FileStorage,
                           location='files')

        data = parse.parse_args()
        article = io.BytesIO(data.get('article').stream.read())

        return stylechecker_validator(article)
