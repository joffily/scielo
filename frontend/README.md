# SciELO - StyleChecker Frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production

If you want to run this project at production enviroment you shall configure the API base url in a local .env file. Before building this project create a `.env` in the root dir with following variable `VUE_APP_ROOT_API=http://...`.

```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Image

![Frontend Screenshot](https://gitlab.com/joffily/scielo/raw/master/frontend/frontend.png)