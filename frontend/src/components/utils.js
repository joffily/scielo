const count_errors = (errors, obj) => {
  errors.map(err => {
    obj[err.message] = obj[err.message] ? obj[err.message] : { lines: [] };
    obj[err.message]["count"] = (obj[err.message]["count"] || 0) + 1;
    obj[err.message]["message"] = err.message;
    obj[err.message]["lines"].push(err.apparent_line);
  });

  return obj
}

export default {
  methods: {
    countStyleErrors(errors) {
      let obj = {};

      if (!errors) {
        return obj;
      }

      Object.keys(errors)
        .map(key => errors[key])
        .map(errors => count_errors(errors, obj));
      return obj;
    },
    countDtdErrors(errors) {
      let obj = {};

      if (!errors) {
        return obj;
      }

      count_errors(errors, obj);
      return obj;
    }
  }
}