import axios from 'axios'

const BASE_URL = process.env.API_BASE_URL ? process.env.API_BASE_URL : 'http://localhost:5000';
const VALIDATE_URL = `${BASE_URL}/validator/stylechecker`
console.log(process.env.VUA_APP_API_BASE_URL)

export default {
  styleChecker: (file, callback, errCallback) => {
    let form = new FormData();
    form.append('article', file);

    axios.post(VALIDATE_URL, form, { 'headers': { 'Content-Type': 'multipart/form-data' } })
      .then(response => {
        if (callback) {
          callback(response);
        }
      })
      .catch(err => {
        if (errCallback) {
          errCallback(err)
        }
      })
  }
}